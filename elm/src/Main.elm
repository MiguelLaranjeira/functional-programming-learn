module Main exposing (..)

import Browser
import Html as Html exposing (..) 

import Http  exposing (..)
import Json.Decode exposing (..)
import Html.Events exposing (onClick)


main = Browser.element 
    { init = init
    , view = view 
    , subscriptions = subscriptions
    , update = update
    }

type ViewModel = 
    Loading
    | Failure
    | Success String
type Action = Fetch
    | GotResp (Result Http.Error String)

init : () -> (ViewModel, Cmd Action)
init _ =
  (Success "", Cmd.none)


subscriptions : ViewModel -> Sub Action
subscriptions model =
  Sub.none

update : Action -> ViewModel ->  (ViewModel, Cmd Action)
update msg model =
    case msg of
        Fetch -> (Loading, getJoke)
        GotResp result ->
            case result of
                Ok resp -> (Success resp, Cmd.none)
                Err _ -> (Failure, Cmd.none)


getJoke : Cmd Action
getJoke = Http.get 
    { url = "https://api.chucknorris.io/jokes/random"
    , expect = Http.expectJson GotResp chuckDecoder
    } 

chuckDecoder: Decoder String
chuckDecoder = field "value" string

view :ViewModel ->  Html Action
view vm = Html.div [] [
    Html.button [ onClick Fetch ] [ Html.text "Get Joke!" ]
    , Html.span [] [ jokeView vm ]
    ] 

jokeView: ViewModel -> Html msg
jokeView vm = 
    case vm of
       Loading -> Html.text "Fetching..."
       Failure -> Html.text "Failed"
       Success resp -> Html.text resp